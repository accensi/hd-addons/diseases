### Notes
- Melee attacks from demonic enemies have a good chance of inflicting a disease on you. Not all of them are deadly, but you will surely feel their effects.
- To treat diseases, pop a stim or drink industrial amounts of blues. Stims don't stop disease progression like blues do, and they work really slowly, but they will fix it eventually.
- If you don't take care of diseases, they will get worse.

### Diseases
- **Ligma**: *inflicted by Hell Nobles. Deals aggravated damage over time. Very deadly.*
- **Coomer's Disease**: *inflicted by Boners. Deals burns over time. Arguably deadlier.*
- **Haemophilia**: *inflicted by Imps. Prevents coagulation. You can only staple/bandage up to a certain point, depending on the disease's progression.*
- **Shakeman's Disease**: *inflicted by Babuins. This makes your aim shaky, making it difficult to aim.*
- **Cataract**: *inflicted by Ninja Pirates. Results in frequent blackouts.*